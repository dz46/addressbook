AddressBook - An symfony application
====================================

System requirements
------------------
  - PHP 7.0
  - PHP-extensions
    - mbstring
    - sqlite3
    - intl


Installation
------------

To install the application simply clone it in to an directory of your choice an run
`composer install`. This will download all needed dependencies. 

To populate the application with demo data you can run:
  - `composer database-create-dev` to create a new database.
  - Followed by `composer fixtures-dev` to insert some dummy data.

Testing
-------
To run the tests on the application, you can run `composer test`, `composer test-text` or `composer test-html`.

When executing `test-text` or `test-html` with `php-xdebug` enabled they will also output how high the coverage is.
`test-html` will also generate HTML-files in the `build/coverage`-directory.

The coverage of the latest built can be found here: https://dz46.gitlab.io/addressbook/coverage/.


Documentation
-------------

The sourcecode also contains phpDoc-comments these can also be rendered to an documentation. To do so you need to
install `graphviz` and after that you can locally install phpDoc with `composer phpdoc-install`. To create the docs
you need to run `composer docs` afterwards. They will be rendered into the `build/docs`-directory.

  
The docs the latest built can be found here: https://dz46.gitlab.io/addressbook/docs/.
