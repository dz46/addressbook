<?php
/**
 * Class AddressBookBundle | AddressBookBundle.php
 */

declare(strict_types=1);

namespace AddressBookBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * The AddressBookBundle it self
 */
class AddressBookBundle extends Bundle
{
}
