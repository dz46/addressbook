<?php

/**
 * Class AbstractBaseController | Controller/AbstractBaseController.php
 */

declare(strict_types=1);

namespace AddressBookBundle\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

/**
 * AbstractBaseController is used as base for the controllers
 * It's used for injecting the needed services via dependency injection
 */
abstract class AbstractBaseController extends Controller
{
    /** @var EntityManager Doctrine EntityManager */
    protected $entityManager;

    /** @var RouterInterface Router component */
    protected $router;

    /** @var Environment Twig */
    protected $twig;

    /** @var FlashBagInterface Session-FlashBag */
    protected $flashBag;

    /**
     * Constructor for dependency injection
     *
     * @param EntityManagerInterface $entityManager
     * @param RouterInterface $router
     * @param Environment $twig
     * @param FlashBagInterface $flashBag
     */
    public function __construct(EntityManagerInterface $entityManager, RouterInterface $router, Environment $twig, FlashBagInterface $flashBag)
    {
        $this->entityManager = $entityManager;
        $this->router = $router;
        $this->twig = $twig;
        $this->flashBag = $flashBag;
    }

    /**
     * This function overrides the original ControllerTrait::redirectToRoute function for better control
     * and availability to override it
     *
     * @param string $route
     * @param array $parameters
     * @param int $status
     *
     * @return RedirectResponse
     */
    public function redirectToRoute($route, array $parameters = [], $status = Response::HTTP_FOUND): RedirectResponse
    {
        return new RedirectResponse($this->router->generate($route, $parameters), $status);
    }

    /**
     * This function overrides the original ControllerTrait::render function for better control
     * and availability to override it
     *
     * @param string $view
     * @param array $parameters
     * @param Response|null $response
     *
     * @return Response
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function render($view, array $parameters = [], Response $response = null): Response
    {
        $content = $this->twig->render($view, $parameters);

        if (null === $response) {
            $response = new Response();
        }

        $response->setContent($content);

        return $response;
    }

    /**
     * Adds a flash message to the current session for type. Overridden for DI
     *
     * @param string $type    The type
     * @param string $message The message
     */
    protected function addFlash($type, $message)
    {
        $this->flashBag->add($type, $message);
    }
}
