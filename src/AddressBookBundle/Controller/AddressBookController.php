<?php

/**
 * Class AddressBookController | Controller/AddressBookController.php
 */

declare(strict_types=1);

namespace AddressBookBundle\Controller;

use AddressBookBundle\Entity\Address;
use AddressBookBundle\Exception\NotFoundHttpException;
use AddressBookBundle\Form\AddressType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Keeps all actions needed for the addressbook
 */
class AddressBookController extends AbstractBaseController
{
    /**
     * This controller lists all names in the database
     *
     * @Route("/addressbook", name="addressbook-list", methods={"GET"})
     */
    public function listAction(): Response
    {
        $entries = $this->entityManager->getRepository(Address::class)->findAllOrderedByLastname();

        return $this->render('addressbook/list.html.twig', [
            'entries' => $entries,
        ]);
    }

    /**
     * Show a single entry
     *
     * @Route("/addressbook/{id}", name="addressbook-edit", methods={"GET", "POST"}, requirements={"id": "\d+"})
     * @Route("/addressbook", name="addressbook-new", methods={"POST"})
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function editOrNewAction(Request $request, int $id = null): Response
    {
        if (null !== $id) {
            // Get element
            $entry = $this->entityManager->getRepository(Address::class)->findOneBy(['id' => $id]);
        } else {
            $entry = new Address();
            $this->entityManager->persist($entry);
        }

        // If not found return 404
        if (null === $entry) {
            throw new NotFoundHttpException($id);
        }

        // create a form but with a request object instead of entity
        $form = $this->createForm(AddressType::class, $entry);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Save the changes to the database
            $this->entityManager->flush();

            // Redirect back to edit-page
            return $this->redirectToRoute('addressbook-edit', ['id' => $entry->getId()]);
        }

        return $this->render('addressbook/entry_edit.html.twig', [
            'entry' => $entry,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Delete an single entry from database
     *
     * @Route("/addressbook/{id}", name="addressbook-delete", methods={"DELETE"}, requirements={"page": "\d+"})
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteAction(Request $request, int $id): Response
    {
        // Get element
        $entry = $this->entityManager->getRepository(Address::class)->findOneBy(['id' => $id]);

        // If not found return 404
        if (null === $entry) {
            throw new NotFoundHttpException($id);
        }

        // Remove element
        $this->entityManager->remove($entry);
        $this->entityManager->flush();

        $this->addFlash(
            'success',
            $entry . ' deleted'
        );

        // Return 200
        return new Response('', Response::HTTP_OK);
    }
}
