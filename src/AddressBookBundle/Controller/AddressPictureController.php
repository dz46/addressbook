<?php

/**
 * Class AddressPictureController | Controller/AddressPictureController.php
 */

declare(strict_types=1);

namespace AddressBookBundle\Controller;

use AddressBookBundle\Entity\Address;
use AddressBookBundle\Exception\NotFoundHttpException;
use AddressBookBundle\Service\AddressPictureService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Keeps all actions needed for the the Picture in addresses
 */
class AddressPictureController extends Controller
{
    /** @var AddressPictureService */
    private $addressPictureService;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * Constructor for dependency injection
     *
     * @param EntityManagerInterface $entityManager
     * @param AddressPictureService $addressPictureService
     */
    public function __construct(EntityManagerInterface $entityManager, AddressPictureService $addressPictureService)
    {
        $this->addressPictureService = $addressPictureService;
        $this->entityManager = $entityManager;
    }

    /**
     * Deletes address picture
     *
     * @Route("/addressbook/{id}/picture", name="addressbook-picture-delete", methods={"DELETE"}, requirements={"id": "\d+"})
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     *
     * @throws \AddressBookBundle\Exception\FileNotDeletableException
     */
    public function deleteAction(Request $request, int $id): Response
    {
        // Fetch the address entity to delete the image from
        $entry = $this->entityManager->getRepository(Address::class)->findOneBy(['id' => $id]);
        // If not found return 404
        if (null === $entry) {
            throw new NotFoundHttpException($id);
        }

        $entry = $this->addressPictureService->removeImage($entry);
        $this->entityManager->flush();

        return new Response(200);
    }

    /**
     * Deletes address picture
     *
     * @Route("/addressbook/{id}/picture", name="addressbook-picture-add", methods={"POST"}, requirements={"id": "\d+"})
     *
     * @param Request $request
     * @param int $id
     *
     * @return Response
     *
     * @throws \AddressBookBundle\Exception\FileNotDeletableException
     * @throws \AddressBookBundle\Exception\FileNotFoundException
     * @throws \AddressBookBundle\Exception\FileNotWritableException
     */
    public function putAction(Request $request, int $id): Response
    {
        // Fetch the address entity to delete the image from
        $entry = $this->entityManager->getRepository(Address::class)->findOneBy(['id' => $id]);

        // If not found return 404
        if (null === $entry) {
            throw new NotFoundHttpException($id);
        }

        $entry = $this->addressPictureService->addImage($request, $entry);
        $this->entityManager->flush();

        $basePath = $this->container->getParameter('pictureUploadFolder');
        $imageLocation = $request->getUriForPath($basePath . $entry->getPictureUrl());
        return new Response('', Response::HTTP_CREATED, [
            'location' => $imageLocation
        ]);
    }
}
