<?php

/**
 * Class AddressBookController | Controller/DefaultController.php
 */

declare(strict_types=1);

namespace AddressBookBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Action for opening the default URL (/)
 */
class DefaultController extends AbstractBaseController
{
    /**
     * Ths controller is only in to redirect to the the addressbook startpage
     *
     * @Route("/", name="homepage")
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function indexAction(Request $request): RedirectResponse
    {
        return $this->redirectToRoute('addressbook-list');
    }
}
