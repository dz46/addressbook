<?php
/**
 * Class AddressFixtures | DataFixtures/AddressFixtures.php
 */

declare(strict_types=1);

namespace AddressBookBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Holds the fixture definition for Address-entity
 */
class AddressFixtures extends Fixture
{
    /**
     * Loads the fixtures created with Nelmio\Alice into the database
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $loader = new \Nelmio\Alice\Loader\NativeLoader();
        $objectSet = $loader->loadFile(__DIR__ . '/fixtures.yml');
        foreach ($objectSet->getObjects() as $object) {
            $manager->persist($object);
        }
        $manager->flush();
    }
}
