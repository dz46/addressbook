<?php
/**
 * Class Address | Entity/Address.php
 */

declare(strict_types=1);

namespace AddressBookBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Address-entity
 *
 * @ORM\Table(name="address")
 * @ORM\Entity(repositoryClass="AddressBookBundle\Repository\AddressRepository")
 */
class Address
{
    /**
     * Id of the entity
     *
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Firstname
     *
     * @var string
     * @Assert\NotBlank
     *
     * @ORM\Column(name="firstname", type="string", length=255)
     */
    private $firstname;

    /**
     * Lastname
     *
     * @var string
     * @Assert\NotBlank
     * @ORM\Column(name="lastname", type="string", length=255)
     */
    private $lastname;

    /**
     * Street and number
     *
     * @var string
     * @Assert\NotBlank
     * @ORM\Column(name="streetAndNumber", type="string", length=255)
     */
    private $streetAndNumber;

    /**
     * Zipcode
     *
     * @var string
     * @Assert\NotBlank
     * @ORM\Column(name="zip", type="string", length=8)
     */
    private $zip;

    /**
     * City
     *
     * @var string
     * @Assert\NotBlank
     * @ORM\Column(name="city", type="string", length=255)
     */
    private $city;

    /**
     * Country
     *
     * @var string
     * @Assert\NotBlank
     * @ORM\Column(name="country", type="string", length=255)
     */
    private $country;

    /**
     * Phonenumber
     *
     * @var string
     * @Assert\NotBlank
     * @ORM\Column(name="phonenumber", type="string", length=255)
     */
    private $phonenumber;

    /**
     * Birthday
     *
     * @var \DateTime
     * @Assert\NotBlank
     * @ORM\Column(name="birthday", type="date")
     */
    private $birthday;

    /**
     * Email address
     *
     * @var string
     *
     * @ORM\Column(name="emailAddress", type="string", length=255)
     * @Assert\Email(
     *     message="The email '{{ value }}' is not a valid email."
     * )
     */
    private $emailAddress;

    /**
     * Picture URL
     *
     * @var string
     *
     * @ORM\Column(name="pictureUrl", type="string", length=255, nullable=true)
     */
    private $pictureUrl;

    /**
     * Get Id
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get firstname
     *
     * @return string|null
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set Firstname
     *
     * @param string $firstname
     *
     * @return self
     */
    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string|null
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set Lastname
     *
     * @param string $lastname
     *
     * @return self
     */
    public function setLastname(string $lastname): self
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get StreetAndNumber
     *
     * @return string|null
     */
    public function getStreetAndNumber()
    {
        return $this->streetAndNumber;
    }

    /**
     * Set StreetAndNumber
     *
     * @param string $streetAndNumber
     *
     * @return self
     */
    public function setStreetAndNumber(string $streetAndNumber): self
    {
        $this->streetAndNumber = $streetAndNumber;

        return $this;
    }

    /**
     * Get Zip
     *
     * @return string|null
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set Zip
     *
     * @param string $zip
     *
     * @return self
     */
    public function setZip(string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get City
     *
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set City
     *
     * @param string $city
     *
     * @return self
     */
    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get Country
     *
     * @return string|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set Country
     *
     * @param string $country
     *
     * @return self
     */
    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get Phonenumber
     *
     * @return string|null
     */
    public function getPhonenumber()
    {
        return $this->phonenumber;
    }

    /**
     * Set Phonenumber
     *
     * @param string $phonenumber
     *
     * @return self
     */
    public function setPhonenumber(string $phonenumber): self
    {
        $this->phonenumber = $phonenumber;

        return $this;
    }

    /**
     * Get Birthday
     *
     * @return \DateTime|null
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set Birthday
     *
     * @param \DateTime $birthday
     *
     * @return self
     */
    public function setBirthday(\DateTime $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get EmailAddress
     *
     * @return string|null
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Set EmailAddress
     *
     * @param string $emailAddress
     *
     * @return self
     */
    public function setEmailAddress(string $emailAddress): self
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * Get PictureUrl
     *
     * @return string|null
     */
    public function getPictureUrl()
    {
        return $this->pictureUrl;
    }

    /**
     * Set setPictureUrl
     *
     * @param string|null $pictureUrl
     *
     * @return self
     */
    public function setPictureUrl($pictureUrl): self
    {
        $this->pictureUrl = $pictureUrl;

        return $this;
    }

    /**
     * Object to string
     *
     * When called output the Firstname & Lastname concatinated
     *
     * @return string|null
     */
    public function __toString(): string
    {
        return $this->firstname . ' ' . $this->lastname;
    }
}
