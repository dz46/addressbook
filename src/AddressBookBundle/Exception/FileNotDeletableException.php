<?php

/**
 * Class FileNotDeletableException | Exception/FileNotDeletableException.php
 */

declare(strict_types=1);

namespace AddressBookBundle\Exception;

/**
 * Class FileNotDeletableException
 */
class FileNotDeletableException extends \Exception
{
    /**
     * FileNotDeletableException constructor
     *
     * @param string $message
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct(string $message = '', int $code = 0, \Throwable $previous = null)
    {
        if ('' !== $message) {
            $message = 'File ' . $message . ' could not be deleted';
        }

        parent::__construct($message, $code, $previous);
    }
}
