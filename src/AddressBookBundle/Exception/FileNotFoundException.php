<?php

/**
 * Class FileNotFoundException | Exception/FileNotFoundException.php
 */

declare(strict_types=1);

namespace AddressBookBundle\Exception;

/**
 * Class FileNotFoundException
 */
class FileNotFoundException extends \Exception
{
    /**
     * FileNotFoundException constructor
     *
     * @param string $message
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct(string $message = '', int $code = 0, \Throwable $previous = null)
    {
        if ('' !== $message) {
            $message = 'File ' . $message . ' not found';
        }

        parent::__construct($message, $code, $previous);
    }
}
