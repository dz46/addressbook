<?php

/**
 * Class FileNotWritableException | Exception/FileNotWritableException.php
 */

declare(strict_types=1);

namespace AddressBookBundle\Exception;

/**
 * Class FileNotWritableException
 */
class FileNotWritableException extends \Exception
{
    /**
     * FileNotWritableException constructor
     *
     * @param string $message
     * @param int $code
     * @param \Throwable|null $previous
     */
    public function __construct(string $message = '', int $code = 0, \Throwable $previous = null)
    {
        if ('' !== $message) {
            $message = 'File ' . $message . ' not writable';
        }

        parent::__construct($message, $code, $previous);
    }
}
