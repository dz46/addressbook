<?php

/**
 * Class NotFoundHttpException | Exception/NotFoundHttpException.php
 */

declare(strict_types=1);

namespace AddressBookBundle\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * NotFoundHttpException Class
 */
class NotFoundHttpException extends HttpException
{
    /**
     * Autogenerate message when called
     *
     * @param int $id The id of the element not found
     * @param \Exception $previous The previous exception
     * @param int $code The internal exception code
     */
    public function __construct(int $id, \Exception $previous = null, $code = 0)
    {
        parent::__construct(Response::HTTP_NOT_FOUND, 'ID: ' . $id . ' not found', $previous, [], $code);
    }
}
