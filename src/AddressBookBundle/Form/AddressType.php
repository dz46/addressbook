<?php

/**
 * Class AddressType | Form/AddressType.php
 */

declare(strict_types=1);

namespace AddressBookBundle\Form;

use AddressBookBundle\Entity\Address;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Form to validate the Address-entity
 */
class AddressType extends AbstractType
{
    /**
     * Build the AddressType for use ins forms
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'firstname',
                TextType::class,
                [
                    'label' => 'First name',
                    'required' => true,
                    'empty_data' => '',
                ]
            )
            ->add(
                'lastname',
                TextType::class,
                [
                    'label' => 'Last name',
                    'required' => true,
                    'empty_data' => '',
                ]
            )
            ->add(
                'streetAndNumber',
                TextType::class,
                [
                    'label' => 'Street and Number',
                    'required' => true,
                    'empty_data' => '',
                ]
            )
            ->add(
                'zip',
                TextType::class,
                [
                    'label' => 'Zip code',
                    'required' => true,
                    'empty_data' => '',
                ]
            )
            ->add(
                'city',
                TextType::class,
                [
                    'label' => 'City',
                    'required' => true,
                    'empty_data' => '',
                ]
            )
            ->add(
                'country',
                TextType::class,
                [
                    'label' => 'County',
                    'required' => true,
                    'empty_data' => '',
                ]
            )
            ->add(
                'phonenumber',
                TextType::class,
                [
                    'label' => 'Phonenumber',
                    'required' => true,
                    'empty_data' => '',
                ]
            )
            ->add(
                'birthday',
                DateType::class,
                [
                    'label' => 'Birthday',
                    'required' => true,
                    'widget' => 'single_text',
                    'format' => 'yyyy-MM-dd',
                ]
            )
            ->add(
                'emailAddress',
                EmailType::class,
                [
                    'label' => 'Email address',
                    'required' => true,
                    'empty_data' => '',
                ]
            );
    }

    /**
     * Set the Address::class as corresponding entity class
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Address::class,
        ]);
    }
}
