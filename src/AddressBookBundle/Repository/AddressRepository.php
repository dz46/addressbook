<?php
/**
 * Class AddressRepository | Controller/AbstractBaseController.php
 */

declare(strict_types=1);

namespace AddressBookBundle\Repository;

/**
 * RepositoryClass for Address Entity
 */
class AddressRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Shortcut mtehod to search for all items sorted by Lastname
     *
     * @return array
     */
    public function findAllOrderedByLastname(): array
    {
        return $this->findBy([], ['lastname' => 'ASC']);
    }
}
