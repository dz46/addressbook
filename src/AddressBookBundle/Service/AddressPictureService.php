<?php

/**
 * Class AddressPictureService | Service/AddressPictureService.php
 */

declare(strict_types=1);

namespace AddressBookBundle\Service;

use AddressBookBundle\Entity\Address;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AddressPictureService
 */
class AddressPictureService
{
    /** @var ContainerInterface Access to symfonies container */
    private $container;

    /** @var FileSystemService Access to filesystem to save files */
    private $fileSystemService;

    /**
     * AddressPictureService constructor.
     *
     * @param ContainerInterface $container
     * @param FileSystemService $fileSystemService
     */
    public function __construct(ContainerInterface $container, FileSystemService $fileSystemService)
    {
        $this->container = $container;
        $this->fileSystemService = $fileSystemService;
        $this->basePath = $this->container->getParameter('pictureUploadFolderAbsolute');
    }

    /**
     * Removes old the old image from filesystem.
     *
     * @param Address $address
     *
     * @return Address
     *
     * @throws \AddressBookBundle\Exception\FileNotDeletableException
     */
    public function removeImage(Address $address): Address
    {
        if ($address->getPictureUrl()) {
            $this->fileSystemService->deleteFile($this->basePath . $address->getPictureUrl());
        }
        $address->setPictureUrl(null);
        return $address;
    }

    /**
     * Saves the uploaded file into the filesystem and edits the Address' entry with the new url
     *
     * Also removes the old image from filesystem if needed
     *
     * @param Request $request
     * @param Address $address
     *
     * @return Address
     *
     * @throws \AddressBookBundle\Exception\FileNotDeletableException
     * @throws \AddressBookBundle\Exception\FileNotFoundException
     * @throws \AddressBookBundle\Exception\FileNotWritableException
     */
    public function addImage(Request $request, Address $address): Address
    {
        // First Check if old image needs to be removed
        $address = $this->removeImage($address);

        $addressId = $address->getId();

        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get('file');
        $extension = $uploadedFile->getClientOriginalExtension();

        $pictureName = uniqid($addressId . '_', true);
        $pictureName = $pictureName . '.' . $extension;

        $this->fileSystemService->moveFile($uploadedFile->getRealPath(), $this->basePath . $pictureName);

        $address->setPictureUrl($pictureName);

        return $address;
    }
}
