<?php

/**
 * Class FileSystemService | Service/FileSystemService.php
 */

declare(strict_types=1);

namespace AddressBookBundle\Service;

use AddressBookBundle\Exception\FileNotDeletableException;
use AddressBookBundle\Exception\FileNotFoundException;
use AddressBookBundle\Exception\FileNotWritableException;

/**
 * Class FileSystemService
 *
 * This class holds all functionality to read, write & delete files from the file system
 */
class FileSystemService
{
    /**
     * Wrapper for file_get_contents
     *
     * @param string $absolutePath
     *
     * @return false|string
     *
     * @throws FileNotFoundException
     */
    public function readFile(string $absolutePath): string
    {
        try {
            $content = \file_get_contents($absolutePath);
        } catch (\Exception $e) {
            throw new FileNotFoundException($absolutePath);
        }

        return $content;
    }

    /**
     * Wrapper for file_put_contents
     *
     * @param string $absolutePath
     * @param $content
     *
     * @return bool|int
     *
     * @throws FileNotWritableException
     */
    public function writeFile(string $absolutePath, $content)
    {
        try {
            $result = \file_put_contents($absolutePath, $content);
        } catch (\Exception $e) {
            throw new FileNotWritableException($absolutePath);
        }

        return $result;
    }

    /**
     * Wrapper for unlink
     *
     * @param string $absolutePath
     *
     * @return bool
     *
     * @throws FileNotDeletableException
     */
    public function deleteFile(string $absolutePath): bool
    {
        try {
            $result = \unlink($absolutePath);
        } catch (\Exception $e) {
            throw new FileNotDeletableException($absolutePath);
        }

        return $result;
    }

    /**
     * Moves a file in the filesystem.
     *
     * @param string $sourceAbsolutePath
     * @param string $destinationAbsolutePath
     *
     * @return bool
     *
     * @throws FileNotFoundException
     * @throws FileNotWritableException
     * @throws FileNotDeletableException
     */
    public function moveFile(string $sourceAbsolutePath, string $destinationAbsolutePath): bool
    {
        $content = $this->readFile($sourceAbsolutePath);
        $this->writeFile($destinationAbsolutePath, $content);
        $this->deleteFile($sourceAbsolutePath);

        return true;
    }
}
