<?php

declare(strict_types=1);

namespace AddressBookBundle\FunctionalTests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @coversDefaultClass \AddressBookBundle\Controller\AddressBookController
 */
class AddressBookControllerTest extends WebTestCase
{
    /**
     * Open the list page
     *
     * @covers ::listAction
     * @covers \AddressBookBundle\Controller\AbstractBaseController
     * @covers \AddressBookBundle\Repository\AddressRepository
     */
    public function testListAction()
    {
        $client = static::createClient();

        // Open page
        $crawler = $client->request('GET', '/addressbook');

        // Check if it was a 200
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        // Find table
        $table = $crawler->filter('table.table.table-striped');

        // Only one table present
        $this->assertCount(1, $table);

        // With 16 entries
        $this->assertCount(16, $table->filter('tbody tr'));
    }

    /**
     * Try to delete existing entry
     *
     * @covers ::deleteAction
     * @covers \AddressBookBundle\Controller\AbstractBaseController
     */
    public function testDeleteActionEntry()
    {
        $client = static::createClient();

        // Call endpoint
        $crawler = $client->request('DELETE', '/addressbook/1');

        // Check if it was a 200
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }

    /**
     * Try to delete not existing entry
     *
     * @covers ::deleteAction
     * @covers \AddressBookBundle\Controller\AbstractBaseController
     */
    public function testDeleteActionNoEntry()
    {
        $client = static::createClient();

        // Call endpoint
        $crawler = $client->request('DELETE', '/addressbook/999');

        // Check if it was a 404
        $this->assertSame(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }

    /**
     * Try to edit existing entry
     *
     * @covers ::editOrNewAction
     * @covers \AddressBookBundle\Controller\AbstractBaseController
     * @covers \AddressBookBundle\Form\AddressType
     */
    public function testEditOrNewActionEntry()
    {
        $client = static::createClient();

        $pageUrl = '/addressbook/2';

        // Call endpoint
        $crawler = $client->request('GET', $pageUrl);

        // Check if it was a 200
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        //Get form
        $form = $crawler->selectButton('Save')->form();

        //Change lastname
        $lastname = uniqid('LastNameTest ', true);
        $form['address[lastname]'] = $lastname;

        // Submit
        $client->submit($form);

        // Check for redirect
        $this->assertSame(Response::HTTP_FOUND, $client->getResponse()->getStatusCode());
        $client->followRedirect();

        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        // Check for redirect to same page
        $this->assertSame('http://localhost' . $pageUrl, $client->getRequest()->getUri());
    }

    /**
     * Try to delete existing entry
     *
     * @covers ::editOrNewAction
     * @covers \AddressBookBundle\Controller\AbstractBaseController
     * @covers \AddressBookBundle\Form\AddressType
     */
    public function testEditOrNewActionEntryEmptyField()
    {
        $client = static::createClient();

        $pageUrl = '/addressbook/3';

        // Call endpoint
        $crawler = $client->request('GET', $pageUrl);

        // Check if it was a 200
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        //Get form
        $form = $crawler->selectButton('Save')->form();

        //Change lastname
        $form['address[lastname]'] = '';

        // Submit
        $crawler = $client->submit($form);

        // Check for page gets reopened
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        // Check for exactly one error
        $errorMessage = $crawler->filter('small.text-danger');
        $this->assertCount(1, $errorMessage);
    }

    /**
     * Try to delete not existing entry
     *
     * @covers ::editOrNewAction
     * @covers \AddressBookBundle\Controller\AbstractBaseController
     * @covers \AddressBookBundle\Form\AddressType
     */
    public function testEditOrNewActionNoEntry()
    {
        $client = static::createClient();

        // Call endpoint
        $crawler = $client->request('GET', '/addressbook/999');

        // Check if it was a 404
        $this->assertSame(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }

    /**
     * Try to add entry
     *
     * @covers ::editOrNewAction
     * @covers \AddressBookBundle\Controller\AbstractBaseController
     * @covers \AddressBookBundle\Form\AddressType
     */
    public function testEditOrNewActiontestNewOrEditActionNewEntry()
    {
        $client = static::createClient();

        $pageUrl = '/addressbook';

        // Call endpoint
        $crawler = $client->request('POST', $pageUrl);

        // Check if it was a 200
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        //Get form
        $form = $crawler->selectButton('Save')->form();

        // Put in each field the name of the field except for birthday and emailAddress
        foreach ($form->getValues() as $field => $value) {
            if (false !== strpos($field, 'emailAddress')) {
                $form[$field] = 'test@email.test';
            } elseif (false !== strpos($field, 'birthday')) {
                $form[$field] = '2000-01-01';
            } else {
                $form[$field] = 'TEST ' . $field;
            }
        }

        // Submit
        $client->submit($form);

        // Check for redirect
        $this->assertSame(Response::HTTP_FOUND, $client->getResponse()->getStatusCode());
        $client->followRedirect();

        // Redirect to new entry
        $this->assertSame(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }
}
