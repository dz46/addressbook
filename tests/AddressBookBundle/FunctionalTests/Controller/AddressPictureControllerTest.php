<?php

declare(strict_types=1);

namespace AddressBookBundle\FunctionalTests\Controller;

use AddressBookBundle\Entity\Address;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

/**
 * @coversDefaultClass \AddressBookBundle\Controller\AddressPictureController
 */
class AddressPictureControllerTest extends WebTestCase
{
    /** @var Client */
    private $client;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var Address */
    private $address;

    /** @var int Id of element created */
    private $addressId;

    protected function setUp()
    {
        $this->client = static::createClient();

        // Boot kernel for access to database
        $kernel = self::bootKernel();

        // Fetch EntityManager
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();

        // Create new entity
        $this->address = new Address();
        $this->address->setFirstname('first')
            ->setLastname('last')
            ->setStreetAndNumber('street 23')
            ->setZip('123454')
            ->setCity('Town')
            ->setCountry('germany')
            ->setPhonenumber('+49 123 45678')
            ->setBirthday(new \DateTime())
            ->setEmailAddress('test@test.test');

        $this->entityManager->persist($this->address);
        $this->entityManager->flush();

        $this->addressId = $this->address->getId();
    }

    protected function tearDown()
    {
        $this->entityManager->remove($this->address);
        $this->entityManager->flush();
        parent::tearDown();
    }

    /**
     * Checks if by default no image is present
     * Uploads one
     * and Removes it again
     *
     * @covers ::putAction
     * @covers ::deleteAction
     */
    public function testPutAndDeleteAction()
    {
        $crawler = $this->client->request('GET', '/addressbook/' . $this->addressId);

        // Check for NO image
        $image = $crawler->filter('img#userPicture');

        // Check no image is present
        $this->assertCount(0, $image);
        // Create image first
        $files = [
            'file' => $this->copyPictureToTest(),
        ];

        // Upload Image
        $crawler = $this->client->request('POST', '/addressbook/' . $this->addressId . '/picture', [], $files);

        $crawler = $this->client->request('GET', '/addressbook/' . $this->addressId);
        $image = $crawler->filter('img#userPicture');

        // Check image is present
        $this->assertCount(1, $image);

        // Now remove the image
        $crawler = $this->client->request('DELETE', '/addressbook/' . $this->addressId . '/picture');

        $crawler = $this->client->request('GET', '/addressbook/' . $this->addressId);
        $image = $crawler->filter('img#userPicture');

        // Check image is present
        $this->assertCount(0, $image);
    }

    /**
     * @covers ::putAction
     */
    public function testPutActionNoEntry()
    {
        $crawler = $this->client->request('POST', '/addressbook/' . 99999999 . '/picture');
        $this->assertSame(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
    }

    /**
     * @covers ::deleteAction
     */
    public function testDeleteActionNoEntry()
    {
        $crawler = $this->client->request('DELETE', '/addressbook/' . 99999999 . '/picture');
        $this->assertSame(Response::HTTP_NOT_FOUND, $this->client->getResponse()->getStatusCode());
    }

    /**
     * Create a demo file for upload
     *
     * @return UploadedFile
     */
    private function copyPictureToTest(): UploadedFile
    {
        $filename_orignal = 'demofile.jpg';
        $filepath_orignal = __DIR__ . '/Files/' . $filename_orignal;

        $filename_test = uniqid('', true) . '.jpg';
        $filepath_test = sys_get_temp_dir() . '/' . $filename_test;

        // Create a copy in /tmp
        copy($filepath_orignal, $filepath_test);

        return new UploadedFile($filepath_test, $filename_orignal);
    }
}
