<?php

declare(strict_types=1);

namespace AddressBookBundle\FunctionalTests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @coversDefaultClass \AddressBookBundle\Controller\DefaultController
 */
class DefaultControllerTest extends WebTestCase
{
    /**
     * @covers ::indexAction
     * @covers \AddressBookBundle\Controller\AbstractBaseController::__construct
     * @covers \AddressBookBundle\Controller\AbstractBaseController::redirectToRoute
     */
    public function testIndexAction()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertSame(Response::HTTP_FOUND, $client->getResponse()->getStatusCode());
        $this->assertSame('/addressbook', $client->getResponse()->headers->get('location'));
    }
}
