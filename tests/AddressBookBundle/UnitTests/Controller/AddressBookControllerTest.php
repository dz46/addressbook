<?php

declare(strict_types=1);

namespace Tests\AddressBookBundle\UnitTests\Controller;

use AddressBookBundle\Controller\AddressBookController;
use AddressBookBundle\Entity\Address;
use AddressBookBundle\Repository\AddressRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @coversDefaultClass \AddressBookBundle\Controller\AddressBookController
 */
class AddressBookControllerTest extends MockInitializer
{
    /**
     * Initialize Mocks
     */
    protected function setUp()
    {
        $this->initializeMocks();
        parent::setUp();
    }

    /**
     * @covers ::listAction
     * @covers \AddressBookBundle\Controller\AbstractBaseController::__construct
     * @covers \AddressBookBundle\Controller\AbstractBaseController::render
     */
    public function testListAction()
    {
        // Add Repository
        $repository = $this->createMock(AddressRepository::class);
        $repository->method('findAllOrderedByLastname')->willReturn([]);

        $this->entityManager->method('getRepository')->willReturn($repository);

        $addressBookController = new AddressBookController($this->entityManager, $this->router, $this->twig, $this->flashBag);

        $response = $addressBookController->listAction();

        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
    }

    /**
     * @covers ::deleteAction
     * @covers \AddressBookBundle\Controller\AbstractBaseController::__construct
     * @expectedException \AddressBookBundle\Exception\NotFoundHttpException
     */
    public function testDeleteActionNoEntry()
    {
        // Add Repository
        $repository = $this->createMock(AddressRepository::class);
        $repository->method('findOneBy')->willReturn(null);

        // EntityManager returns the repository
        $this->entityManager->method('getRepository')->willReturn($repository);

        $addressBookController = new AddressBookController($this->entityManager, $this->router, $this->twig, $this->flashBag);

        $response = $addressBookController->deleteAction(new Request(), 999);

        $this->assertSame(Response::HTTP_NOT_FOUND, $response->getStatusCode());
    }

    /**
     * @covers ::deleteAction
     * @covers \AddressBookBundle\Controller\AbstractBaseController::__construct
     * @covers \AddressBookBundle\Controller\AbstractBaseController::addFlash
     */
    public function testDeleteActionEntry()
    {
        // Add Repository
        $repository = $this->createMock(AddressRepository::class);
        $repository->method('findOneBy')->willReturn(new Address());

        // EntityManager returns the repository
        $this->entityManager->method('getRepository')->willReturn($repository);

        $addressBookController = new AddressBookController($this->entityManager, $this->router, $this->twig, $this->flashBag);

        $response = $addressBookController->deleteAction(new Request(), 1);

        $this->assertSame(Response::HTTP_OK, $response->getStatusCode());
    }
}
