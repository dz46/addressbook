<?php

declare(strict_types=1);

namespace AddressBookBundle\UnitTests\Controller;

use AddressBookBundle\Controller\AddressPictureController;
use AddressBookBundle\Entity\Address;
use AddressBookBundle\Repository\AddressRepository;
use AddressBookBundle\Service\AddressPictureService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @coversDefaultClass \AddressBookBundle\Controller\AddressPictureController
 */
class AddressPictureControllerTest extends TestCase
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var AddressPictureService */
    private $addressPictureService;

    /** @var AddressPictureController */
    private $addressPictureController;

    /** @var AddressRepository */
    private $repository;

    /** @var ContainerInterface */
    private $container;

    protected function setUp()
    {
        $this->entityManager = $this->createMock(EntityManagerInterface::class);
        $this->addressPictureService = $this->createMock(AddressPictureService::class);

        $this->container = $this->createMock(ContainerInterface::class);

        $this->addressPictureController = new AddressPictureController($this->entityManager, $this->addressPictureService);
        $this->addressPictureController->setContainer($this->container);

        $this->repository = $this->createMock(AddressRepository::class);
        $this->entityManager
            ->method('getRepository')
            ->willReturn($this->repository);
        parent::setUp();
    }

    /**
     * @covers ::putAction
     * @covers ::__construct
     *
     * @throws \AddressBookBundle\Exception\FileNotDeletableException
     * @throws \AddressBookBundle\Exception\FileNotFoundException
     * @throws \AddressBookBundle\Exception\FileNotWritableException
     */
    public function testPutAction()
    {
        $entry = new Address();

        $this->repository
            ->method('findOneBy')
            ->willReturn($entry);

        $files = [
            'file' => [
                'name' => 'test',
                'type' => 'image/jpeg',
                'tmp_name' => 'test',
            ],
        ];

        $request = new Request([], [], [], [], $files);

        $result = $this->addressPictureController->putAction($request, 5);

        $this->assertSame(Response::HTTP_CREATED, $result->getStatusCode());
    }

    /**
     * @covers ::__construct
     * @covers ::putAction
     * @expectedException \AddressBookBundle\Exception\NotFoundHttpException
     */
    public function testPutActionNoEntry()
    {
        $this->repository
            ->method('findOneBy')
            ->willReturn(null);

        $request = new Request();

        $result = $this->addressPictureController->putAction($request, 5);
    }

    /**
     * @covers ::__construct
     * @covers ::deleteAction
     */
    public function testDeleteAction()
    {
        $entry = new Address();

        $this->repository
            ->method('findOneBy')
            ->willReturn($entry);

        $result = $this->addressPictureController->deleteAction(new Request(), 5);

        $this->assertSame(Response::HTTP_OK, $result->getStatusCode());
    }

    /**
     * @covers ::__construct
     * @covers ::deleteAction
     * @expectedException \AddressBookBundle\Exception\NotFoundHttpException
     */
    public function testDeleteActionNoEntry()
    {
        $this->repository
            ->method('findOneBy')
            ->willReturn(null);

        $result = $this->addressPictureController->deleteAction(new Request(), 5);

        $this->assertSame(Response::HTTP_OK, $result->getStatusCode());
    }
}
