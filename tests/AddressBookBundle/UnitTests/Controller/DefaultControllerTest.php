<?php

declare(strict_types=1);

namespace Tests\AddressBookBundle\UnitTests\Controller;

use AddressBookBundle\Controller\DefaultController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @coversDefaultClass \AddressBookBundle\Controller\DefaultController
 */
class DefaultControllerTest extends MockInitializer
{
    /**
     * Initialize Mocks
     */
    protected function setUp()
    {
        $this->initializeMocks();
        parent::setUp();
    }

    /**
     * @covers ::indexAction
     * @covers \AddressBookBundle\Controller\AbstractBaseController::__construct
     * @covers \AddressBookBundle\Controller\AbstractBaseController::redirectToRoute
     */
    public function testIndexAction()
    {
        $this->router->method('generate')->willReturn('/addressbook');

        $defaultController = new DefaultController($this->entityManager, $this->router, $this->twig, $this->flashBag);

        $response = $defaultController->indexAction(new Request());

        $this->assertSame(Response::HTTP_FOUND, $response->getStatusCode());
        $this->assertSame('/addressbook', $response->headers->get('location'));
    }
}
