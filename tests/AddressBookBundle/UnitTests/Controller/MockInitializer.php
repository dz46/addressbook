<?php

declare(strict_types=1);

namespace Tests\AddressBookBundle\UnitTests\Controller;

use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;

class MockInitializer extends TestCase
{
    /** @var MockObject */
    protected $entityManager;

    /** @var MockObject */
    protected $router;

    /** @var MockObject */
    protected $twig;

    /** @var MockObject */
    protected $flashBag;

    /**
     * Creates the default mocks needed for the unit tests
     *
     * @param EntityManagerInterface|null $entityManager
     * @param RouterInterface|null $router
     * @param Environment|null $twig
     * @param FlashBagInterface|null $flashBag
     */
    public function initializeMocks(
        EntityManagerInterface $entityManager = null,
        RouterInterface $router = null,
        Environment $twig = null,
        FlashBagInterface $flashBag = null
    ) {
        $this->entityManager = $entityManager;
        if (null === $entityManager) {
            $this->entityManager = $this->createMock(EntityManagerInterface::class);
        }

        $this->router = $router;
        if (null === $router) {
            $this->router = $this->createMock(RouterInterface::class);
        }

        $this->twig = $twig;
        if (null === $twig) {
            $this->twig = $this->createMock(Environment::class);
        }

        $this->flashBag = $flashBag;
        if (null === $flashBag) {
            $this->flashBag = $this->createMock(FlashBagInterface::class);
        }
    }
}
