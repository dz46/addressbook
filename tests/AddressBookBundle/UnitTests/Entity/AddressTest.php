<?php

declare(strict_types=1);

namespace Tests\AddressBookBundle\UnitTests\Entity;

use AddressBookBundle\Entity\Address;
use PHPUnit\Framework\TestCase;

class AddressTest extends TestCase
{
    /** @var Address */
    private $address;

    /** @var string */
    private $uniqVal;

    protected function setUp()
    {
        parent::setUp();
        $this->address = new Address();
        $this->uniqVal = uniqid('', true);
    }

    public function testGetId()
    {
        $this->assertNull($this->address->getId());
    }

    public function testSetGetFirstname()
    {
        $this->address->setFirstname($this->uniqVal);
        $this->assertSame($this->uniqVal, $this->address->getFirstname());
    }

    public function testSetGetLastname()
    {
        $this->address->setLastname($this->uniqVal);
        $this->assertSame($this->uniqVal, $this->address->getLastname());
    }

    public function testSetGetStreetAndNumber()
    {
        $this->address->setStreetAndNumber($this->uniqVal);
        $this->assertSame($this->uniqVal, $this->address->getStreetAndNumber());
    }

    public function testSetGetZip()
    {
        $this->address->setZip($this->uniqVal);
        $this->assertSame($this->uniqVal, $this->address->getZip());
    }

    public function testSetGetCity()
    {
        $this->address->setCity($this->uniqVal);
        $this->assertSame($this->uniqVal, $this->address->getCity());
    }

    public function testSetGetCountry()
    {
        $this->address->setCountry($this->uniqVal);
        $this->assertSame($this->uniqVal, $this->address->getCountry());
    }

    public function testSetGetPhonenumber()
    {
        $this->address->setPhonenumber($this->uniqVal);
        $this->assertSame($this->uniqVal, $this->address->getPhonenumber());
    }

    public function testSetGetEmailAddress()
    {
        $this->address->setEmailAddress($this->uniqVal);
        $this->assertSame($this->uniqVal, $this->address->getEmailAddress());
    }

    public function testSetGetBirthday()
    {
        $this->uniqVal = new \DateTime();
        $this->uniqVal->setDate(1970, 1, 1);
        $this->address->setBirthday($this->uniqVal);
        $this->assertSame($this->uniqVal, $this->address->getBirthday());
    }

    public function testSetGetPictureUrl()
    {
        $this->address->setPictureUrl($this->uniqVal);
        $this->assertSame($this->uniqVal, $this->address->getPictureUrl());
    }

    public function testToString()
    {
        $this->address->setFirstname($this->uniqVal);
        $this->address->setLastname($this->uniqVal);
        $this->assertSame($this->uniqVal . ' ' . $this->uniqVal, $this->address->__toString());
    }
}
