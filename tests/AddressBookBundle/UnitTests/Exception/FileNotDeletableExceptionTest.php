<?php

declare(strict_types=1);

namespace Tests\AddressBookBundle\UnitTests\Exception;

use AddressBookBundle\Exception\FileNotDeletableException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \AddressBookBundle\Exception\FileNotDeletableException
 */
class FileNotDeletableExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     *
     * @throws \Exception
     */
    public function test__construct()
    {
        $fileName = \uniqid('file_', true);
        $exception = new FileNotDeletableException($fileName);
        $this->assertSame('File ' . $fileName . ' could not be deleted', $exception->getMessage());
    }
}
