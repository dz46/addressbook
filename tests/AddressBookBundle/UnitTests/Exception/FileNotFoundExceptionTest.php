<?php

declare(strict_types=1);

namespace Tests\AddressBookBundle\UnitTests\Exception;

use AddressBookBundle\Exception\FileNotFoundException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \AddressBookBundle\Exception\FileNotFoundException
 */
class FileNotFoundExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     *
     * @throws \Exception
     */
    public function test__construct()
    {
        $fileName = \uniqid('file_', true);
        $exception = new FileNotFoundException($fileName);
        $this->assertSame('File ' . $fileName . ' not found', $exception->getMessage());
    }
}
