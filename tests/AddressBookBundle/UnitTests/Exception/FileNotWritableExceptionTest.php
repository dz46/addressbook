<?php

declare(strict_types=1);

namespace Tests\AddressBookBundle\UnitTests\Exception;

use AddressBookBundle\Exception\FileNotWritableException;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \AddressBookBundle\Exception\FileNotWritableException
 */
class FileNotWritableExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     *
     * @throws \Exception
     */
    public function test__construct()
    {
        $fileName = \uniqid('file_', true);
        $exception = new FileNotWritableException($fileName);
        $this->assertSame('File ' . $fileName . ' not writable', $exception->getMessage());
    }
}
