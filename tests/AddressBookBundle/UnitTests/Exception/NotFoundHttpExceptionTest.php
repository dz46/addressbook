<?php

declare(strict_types=1);

namespace Tests\AddressBookBundle\UnitTests\Exception;

use AddressBookBundle\Exception\NotFoundHttpException;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @coversDefaultClass \AddressBookBundle\Exception\NotFoundHttpException
 */
class NotFoundHttpExceptionTest extends TestCase
{
    /**
     * @covers ::__construct
     *
     * @throws \Exception
     */
    public function test__construct()
    {
        $id = \random_int(10, 99);
        $exception = new NotFoundHttpException($id);

        $this->assertSame(Response::HTTP_NOT_FOUND, $exception->getStatusCode());
        $this->assertSame('ID: ' . $id . ' not found', $exception->getMessage());
    }
}
