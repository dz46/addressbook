<?php

declare(strict_types=1);

namespace Tests\AddressBookBundle\UnitTests\Form;

use AddressBookBundle\Entity\Address;
use AddressBookBundle\Form\AddressType;
use Symfony\Component\Form\Test\TypeTestCase;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressTypeTest extends TypeTestCase
{
    public function testConfigureOptions()
    {
        $formData = [
            'firstname' => 'test',
            'lastname' => 'test2',
            'streetAndNumber' => 'test3',
            'zip' => 'test4',
            'city' => 'test7',
            'country' => 'test8',
            'phonenumber' => 'test5',
            'birthday' => '2010-01-01',
            'emailAddress' => 'test6',
        ];

        // Initialize new Object
        $address = new Address();
        $address->setFirstname('first')
            ->setLastname('last')
            ->setStreetAndNumber('streetAndNumber')
            ->setZip('zip')
            ->setCity('city')
            ->setCountry('country')
            ->setPhonenumber('12346')
            ->setBirthday(new \DateTime())
            ->setEmailAddress('test@test.test');

        // Create the form & submit
        $form = $this->factory->create(AddressType::class, $address);
        $form->submit($formData);

        $this->assertTrue($form->isSynchronized());

        // Create the view to check output
        $view = $form->createView();
        $children = $view->children;

        // Check if all fields from $formData are contained
        foreach (array_keys($formData) as $key) {
            $this->assertArrayHasKey($key, $children);
        }

        // Check if data was successfully applied to the object
        foreach ($children  as $child) {
            // Special Check for DateTimeFields
            if ($child->vars['data'] instanceof \DateTime) {
                $this->assertEquals($formData[$child->vars['name']], $child->vars['data']->format('Y-m-d'));
            } else {
                $this->assertEquals($formData[$child->vars['name']], $child->vars['data']);
            }
        }
    }

    public function testBuildForm()
    {
        $optionsResolver = $this->createMock(OptionsResolver::class);
        $optionsResolver->expects($this->once())->method('setDefaults')->with(['data_class' => Address::class]);

        $addressType = new AddressType();
        $addressType->configureOptions($optionsResolver);
    }
}
