<?php

declare(strict_types=1);

namespace AddressBookBundle\UnitTests\Service;

use AddressBookBundle\Entity\Address;
use AddressBookBundle\Service\AddressPictureService;
use AddressBookBundle\Service\FileSystemService;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

/**
 * @coversDefaultClass \AddressBookBundle\Service\AddressPictureService
 */
class AddressPictureServiceTest extends TestCase
{
    /** @var FileSystemService */
    private $fss;

    /** @var ContainerInterface */
    private $container;

    protected function setUp()
    {
        $this->fss = $this->createMock(FileSystemService::class);
        $this->container = $this->createMock(ContainerInterface::class);

        // Setup the getParameter function
        $this->container
            ->method('getParameter')
            ->with('pictureUploadFolderAbsolute')
            ->willReturn('test');

        parent::setUp();
    }

    /**
     * @covers ::__construct
     * @covers ::removeImage
     */
    public function testRemoveImage()
    {
        $address = new Address();
        $address->setPictureUrl('testImage');

        $aps = new AddressPictureService($this->container, $this->fss);
        $aps->removeImage($address);
        $this->assertNull($address->getPictureUrl());
    }

    /**
     * @covers ::__construct
     * @covers ::removeImage
     */
    public function testRemoveImageNoImage()
    {
        $address = new Address();

        $aps = new AddressPictureService($this->container, $this->fss);
        $aps->removeImage($address);
        $this->assertNull($address->getPictureUrl());
    }

    /**
     * @covers ::__construct
     * @covers ::addImage
     */
    public function testAddImage()
    {
        $address = new TestAddress();
        $addressId = 999;
        $address->setId($addressId);
        $aps = new AddressPictureService($this->container, $this->fss);
        $this->assertNull($address->getPictureUrl());

        // Create DemoFile
        $filename = sys_get_temp_dir() . '/demofile';
        file_put_contents($filename, uniqid('', true));
        $fileExtension = 'jpg';
        $file = new UploadedFile($filename, 'image.' . $fileExtension);

        $request = new Request();

        $request->files->set('file', $file);

        $address = $aps->addImage($request, $address);

        $this->assertStringStartsWith((string) $addressId, $address->getPictureUrl());
        $this->assertStringEndsWith($fileExtension, $address->getPictureUrl());
    }
}

/**
 * Class testAddress
 *
 * Needed for testing;
 * Needed for testing;
 */
class TestAddress extends Address
{
    /** @var int */
    private $_id;

    /**
     * Sets the ID for testing
     *
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->_id = $id;
    }

    /**
     * Gets the ID for testing
     *
     * @return int
     */
    public function getId()
    {
        return $this->_id;
    }
}
