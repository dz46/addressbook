<?php

declare(strict_types=1);

namespace AddressBookBundle\UnitTests\Service;

use AddressBookBundle\Service\FileSystemService;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \AddressBookBundle\Service\FileSystemService
 */
class FileSystemServiceTest extends TestCase
{
    /** @var FileSystemService */
    private $fss;

    protected function setUp()
    {
        // Instantiate new FileSystemService
        $this->fss = new FileSystemService();
        parent::setUp();
    }

    /**
     * @covers ::writeFile
     */
    public function testWriteFile()
    {
        $filename = sys_get_temp_dir() . '/test_writefile';
        $content = uniqid('', true);
        $this->fss->writeFile($filename, $content);
        $this->assertSame($content, file_get_contents($filename));

        //Cleanup
        $this->fss->deleteFile($filename);
    }

    /**
     * @covers ::writeFile
     * @expectedException \AddressBookBundle\Exception\FileNotWritableException
     */
    public function testWriteFileException()
    {
        $content = uniqid('', true);
        $filename = $content . '/test_writefile';
        $this->fss->writeFile($filename, $content);
        $this->assertSame($content, file_get_contents($filename));
    }

    /**
     * @covers ::readFile
     */
    public function testReadFile()
    {
        $filename = sys_get_temp_dir() . '/test_readfile';
        $content = uniqid('', true);
        $this->fss->writeFile($filename, $content);

        $this->assertSame($content, $this->fss->readFile($filename));

        //Cleanup
        $this->fss->deleteFile($filename);
    }

    /**
     * @covers ::readFile
     * @expectedException \AddressBookBundle\Exception\FileNotFoundException
     */
    public function testReadFileException()
    {
        $content = uniqid('', true);
        $filename = $content . '/test_readfile';
        $this->fss->readFile($filename);
    }

    /**
     * @covers ::deleteFile
     */
    public function testDeleteFile()
    {
        $filename = sys_get_temp_dir() . '/test_deletefile';
        $content = uniqid('', true);
        $this->fss->writeFile($filename, $content);
        $this->assertFileExists($filename);
        $this->assertSame($content, file_get_contents($filename));
        $this->fss->deleteFile($filename);
        $this->assertFileNotExists($filename);
    }

    /**
     * @covers ::deleteFile
     * @expectedException \AddressBookBundle\Exception\FileNotDeletableException
     */
    public function testDeleteFileException()
    {
        $filename = sys_get_temp_dir() . '/' . uniqid('', true);
        $this->assertFileNotExists($filename);
        $this->fss->deleteFile($filename);
    }

    /**
     * @covers ::moveFile
     */
    public function testMoveFile()
    {
        $oldFilename = sys_get_temp_dir() . '/test_movefile';
        $newFilename = sys_get_temp_dir() . '/test_movefile2';
        $content = uniqid('', true);
        $this->fss->writeFile($oldFilename, $content);
        $this->assertFileExists($oldFilename);
        $this->assertFileNotExists($newFilename);

        $result = $this->fss->moveFile($oldFilename, $newFilename);

        $this->assertFileExists($newFilename);
        $this->assertFileNotExists($oldFilename);

        $this->assertSame($content, $this->fss->readFile($newFilename));

        $this->assertTrue($result);

        //Cleanup
        $this->fss->deleteFile($newFilename);
    }
}
