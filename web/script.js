$(function () {
    $('body.addressbook-list tr.address-item a.action-delete').click(function (e) {

        $url = $(this).closest('tr').data('url');
        $.ajax({
            url: $url,
            type: 'DELETE',
            success: function (result) {
                location.reload();
            }
        });
    });

    $("body.addressbook-edit #file").on('change', (function (e) {
        $(this).closest('form').submit();
    }));

    $("body.addressbook-edit #uploadimage").on('submit', (function (e) {
        e.preventDefault();
        $.ajax({
            url: $(this).data('upload-url'),
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data, status, xhr) {
                // Change the image
                $('img.userPicture').attr('src', xhr.getResponseHeader('Location'))
            }, error: function (data) {
                console.error(data);
            }
        });
    }));
});